# Deployment

Deployment jobs are part of the 'Deployment.gitlab-ci.yml' template, which will be added/extended in your pipeline through the `Application.gitlab-ci.yml`.

## .pages-deploy

This job deploys GitLab Pages. For more information see [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

**Usage**

```yaml
pages:
  extends: .pages-deploy
  stage: deploy
  script:
    - mkdir public
    - mv documentation/* public/
```

**Image**: *default image*

**No Variables**

---

## .skopeo-copy

This job runs a `skopeo copy` command to copy an image from one location to another while maintaining the digest.

Skopeo is a tool for moving container images between different types of container storages. It allows you to copy container images between container registries like docker.io, quay.io, and your internal container registry or different types of storage on your local system. For more info please see [skopeo](https://github.com/containers/skopeo).

**Usage**

```yaml
promote-image:
  extends: .skopeo-copy
  stage: deploy
  variables:
    SRC_USER: ${CI_REGISTRY_USER}
    SRC_PASS: ${CI_REGISTRY_PASSWORD}
    DEST_USER: ${CI_REGISTRY_USER}
    DEST_PASS: ${CI_REGISTRY_PASSWORD}
    SRC_IMAGE: ${CI_REGISTRY_IMAGE}
    SRC_TAG: 'latest'
    DEST_IMAGE: ${CI_REGISTRY_IMAGE}
    DEST_TAG: 'latest'
```

**Image**: *custom-registry*/skopeo-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|SRC_USER|'$CI_REGISTRY_USER'|User name for the source container registry|
|SRC_PASS|'$CI_REGISTRY_PASSWORD'|Password or token for the source container registry|
|DEST_USER|'$CI_REGISTRY_USER'|User name for the destination container registry|
|DEST_PASS|'$CI_REGISTRY_PASSWORD'|Password or token for the destination container registry|
|SRC_IMAGE|'$CI_REGISTRY_IMAGE'|The source image is the image path without the tag. i.e. registry.gitlab.com/group/project|
|SRC_TAG|'latest'|The tag associated with the source image. i.e. latest or 1.1.0|
|DEST_IMAGE|'$CI_REGISTRY_IMAGE'|The destination image is the image path without the tag. i.e. registry.gitlab.com/group/project|
|DEST_TAG|'latest'|The tag associated with the destination image. i.e. latest or 1.1.0|
  
---

## .cloudformation-deploy

This job enacts a `aws cloudformation deploy` command to deploy a cloudformation template. For more information, see [AWS CLI Command Reference | cloudformation | deploy](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/cloudformation/deploy/index.html).

**Usage**

```yaml
cft-deploy:
  stage: deploy
  environment:
    name: $ENV_NAME
  extends: .cloudformation-deploy
  variables:
    CFN_TEMPLATE_FILE: './cloudformation/cf-template.yml'
    CFN_STACKNAME: '$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA'
    CFN_CAPABILITIES: 'CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND'
    CFN_PARAMETER_OVERRIDES: 'file:/config/cf-parameters.json'
    CFN_S3_BUCKET: 'my-bucket'
```

**Image**: *custom-registry*/aws-cli-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|CFN_TEMPLATE_FILE|'./cloudformation/cf-template.yml'|The path where your AWS CloudFormation template is located|
|CFN_STACKNAME|''|The name of the AWS CloudFormation stack you’re deploying to. If you specify an existing stack, the command updates the stack. If you specify a new stack, the command creates it.|
|CFN_CAPABILITIES|'CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND'|A list of capabilities that you must specify before AWS Cloudformation can create certain stacks.|
|CFN_PARAMETER_OVERRIDES|'file:/config/cf-parameters.json'|A list of parameter structures that specify input parameters for your stack template.|
|CFN_S3_BUCKET|''|The name of the S3 bucket where this command uploads your CloudFormation template.|

---

## .docker-pull

This job conducts a `docker pull` then a `docker push` to pull an image down and push it into a new image registry. This image contains the ECR Credential Helper for AWS. The optional `AWS_ACCOUNT` and `AWS_REGION` variables will be automatically written to the `~/.docker/config.json` file to enable the ECR Helper for any AWS Items. For more information, see [docker pull](https://docs.docker.com/engine/reference/commandline/pull/), [docker push](https://docs.docker.com/engine/reference/commandline/push/), and [Amazon ECR Docker Credential Helper](https://github.com/awslabs/amazon-ecr-credential-helper#amazon-ecr-docker-credential-helper).

*This template can be used without AWS*

**Usage**

```yaml
docker-pull:
  stage: deploy
  extends: .docker-pull
  variables:
    PULL_IMAGE: "my-dev-image"
    PUSH_IMAGE: "my-prod-image"
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""
    AWS_ACCOUNT: "123456789012"
    AWS_REGION: "us-east-1"
    REGISTRY: $CI_REGISTRY
    REGISTRY_USER: $CI_REGISTRY_USER
    REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
```

**Image**: *custom-registry*/docker-ecr-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|PULL_IMAGE|""| This is the image that is being pulled down. |
|PUSH_IMAGE|""| This is the path that we are pushing the docker image too. |
|DOCKER_HOST|"tcp://docker:2375"| This is necessary to connect to the docker service. |
|DOCKER_TLS_CERTDIR|""| Not currently used, but if an org wants to enable TLS Between docker services, it can be used. |
|AWS_ACCOUNT|""| (Optional) This is written to `~/.docker/config.json` to enable the ECR Credential Helper. |
|AWS_REGION|""| (Optional) This is written to `~/.docker/config.json` to enable the ECR Credential Helper. |

---

## .s3-deploy

This job enacts a `aws s3 sync` command to deploy a a directory to S3. For more information, see [AWS CLI Command Reference | s3 | sync](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/sync.html).

**Usage**

```yaml
s3-deploy:
  stage: deploy
  extends: .s3-deploy
  variables:
    SYNC_DIR: './uploads'
    S3_BUCKET: 'my-bucket'
    S3_PREFIX: 'files/uploads'
```

**Image**: *custom-registry*/aws-cli-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|SYNC_DIR|''| The directory to upload to S3. |
|S3_BUCKET|''| The S3 Bucket to upload the artifact too. |
|S3_PREFIX|''| The S3 Path. i.e. "myimage/myimage.tar" |

---

## .release-job

This job enacts a `release cli create` command to create a release in GitLab. For more information, see [release cli](https://gitlab.com/gitlab-org/release-cli).

**Usage**

```yaml
create-release:
  stage: deploy
  extends: .release-job
  variables:
    TITLE: '$TAG - $SHORT-DESC'
    DESC: '$RELEASE NOTES'
    VERSION: '$TAG'
```

**Image**: *custom-registry*/release-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|TITLE|''|The release name.|
|DESC|''|The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string.|
|VERSION|''|The tag the release will be created from.|

---

## .update-changelog

Available since [13.9](https://about.gitlab.com/releases/2021/02/22/gitlab-13-9-released/#create-changelogs-using-the-gitlab-api),
generate changelog data based on commits in a repository. Commits must be in a [standard format](https://docs.gitlab.com/ee/api/repositories.html#add-changelog-data-to-a-changelog-file). For more information, see [CHangelog entries](https://docs.gitlab.com/ee/development/changelog.html).

**Usage**

```yaml
update-changelog:
  stage: deploy
  extends: .update-changelog
  variables:
    VERSION: '$TAG'
```

**Image**: *default image*

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|VERSION|''|The version to generate the changelog for. The format must follow semantic versioning.|

---

## .gitversion_function

[GitVersion](https://gitversion.net/) is a tool that generates a Semantic Version number based on your Git history. This job is based off of [Utterly Automated Software and Artifact Versioning with GitVersion](https://gitlab.com/guided-explorations/devops-patterns/utterly-automated-versioning). It is recommended to only use with protected branches. The output is a variable `$PACKAGE_VERSION`.

**Usage**

```yaml
set-version:
  stage: deploy
  extends: .gitversion_function
  variables:
    GIT_STRATEGY: none
```

**Image**: [gittools/gitversion](https://hub.docker.com/r/gittools/gitversion)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|[GIT_STRATEGY](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-strategy)|'none'|A Git strategy of `none` re-uses the local working copy, but skips all Git operations normally done by GitLab.|
