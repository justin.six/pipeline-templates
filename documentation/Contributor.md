# Introduction

Rule of thumb, when in doubt submit a Merge Request! We'll work out all the details from there, and work with you to get it included in the Pipeline COE. Below are some of the things we're looking for.

## Getting Started 

Please feel free to contribute by simply creating an issue. Otherwise, if you're looking to make changes, feel free to submit a merge request. 

**Helpful Tip**: Use the command `npm run serve` at the root directory, to spin up a local server to view and test your changes before committing. You may need to run `npm install` first.

## No Bash/Shell Scripts inside .gitlab-ci.yml

Bash/Shell scripts inside of .gitlab-ci.yml files are difficult to read and process. Most IDEs won't syntax highlight them properly. They're more difficult to read, etc.

The best approach should be to write a container containing all of your bash/shell scripts. You can even bundle them inside of the Base Container if desired, but understand if they go there, they'll be in almost every container. We'd prefer any bash/shell scripts be in an container in the `/opt/pipelinecoe/` folder.
