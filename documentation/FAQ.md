# Frequently Asked Questions

## General Questions

+ What is the Pipeline COE? +
	
	The Pipeline COE is a collection of tools, containers, scripts, and patterns that enable a customer to go from 0 to Hero on GitLab CI in a small period of time. It's goal is to reduce the time to value of any Digital Transformation involving GitLab CI. It includes everything a customer will need to begin their GitLab CI journey; yet it is built in a modular way to support each customer's unique requirements.

+ Who is the Pipeline COE for? +

+ What are the pros & cons of using the Pipeline COE custom template approach versus using Auto DevOps? +

    Auto DevOps is a quick way to have a pipeline built based on automatic detection of files inside your repository. One of the success paths of Auto DevOps leverages the CNCF buildpacks and our default Helm app chart. Auto DevOps only supports Kubernetes/ECS Deployments. If you need to deploy somewhere else, Auto DevOps does not support it. Auto DevOps also does not integrate with your third party security tools. Auto DevOps best serves customers who have little to no experience building pipelines and are looking to build a wide array of basic examples for learning purposes and/or as a template for simple projects with little need for configuration, customization, or updating. 

    The Pipeline COE brings Pipeline Templates that lowers the barrier of entry to making your own pipeline. It also brings governance, hardened containers, and documentation on best practices. The Pipeline COE allows you to control what containers are being used for the pipeline. You control everything that runs in your pipeline from start to finish. The Pipeline COE best serves customers who are looking to master the art of building both simple and advanced pipelines that can be tailored to their needs based on tech stack, dependencies, infrastructure requirements, project complexity, etc. 

## Supply Chain Questions

+ Why did you decide to use RHEL UBI Containers as a base? +

  TODO

+ Why are there so many containers? +

  TODO

+ Why are these containers not used to deploy applications? +

  TODO

+ How are all of these containers managed? +

  TODO
