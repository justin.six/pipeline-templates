# Documentation available at https://gitlab-org.gitlab.io/professional-services-automation/pipelinecoe/pipeline-templates/#/PipelineTemplates/Terraform

.terraform-job: &terraform
  image:
    name: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  variables:
    TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
    TF_STATE_NAME: ${TF_STATE_NAME:-default}  # The name of the state file used by the GitLab Managed Terraform state backend

  cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/
    - ${TF_ROOT}/.terraform.lock.hcl
  
.terraform-fmt: &fmt
  extends: .terraform-job
  stage: validate
  script:
    - cd "${TF_ROOT}"
    - gitlab-terraform fmt
  allow_failure: true

.terraform-init: &init
  extends: .terraform-job
  stage: init
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform init

.terraform-validate: &validate
  extends: .terraform-job
  stage: validate
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform validate

.terraform-build: &build
  extends: .terraform-job
  stage: build
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

.terraform-deploy: &deploy
  extends: .terraform-job
  stage: deploy
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform apply
  resource_group: ${TF_STATE_NAME}
  when: manual
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.terraform-destroy: &destroy
  extends: .terraform-job
  stage: cleanup
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform destroy
  resource_group: ${TF_STATE_NAME}
  when: manual

.terraform-unlock: &unlock
  extends: .terraform-job
  stage: deploy
  needs: []
  script:
    - 'curl -X DELETE --header "PRIVATE-TOKEN: ${CI_JOB_TOKEN}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock'
  when: manual
